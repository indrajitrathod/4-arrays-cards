const cards = require('./cardDetails');

const getsCardsSumOfEvenPositionIsOdd = ((cards) => {
    const cardsWithSumofEvenPositionOdd = cards.filter((card) => {

        const sumOfEvenPosition = card['card_number'].split('').reduce((sum, cardDigit, index) => {
            sum += index % 2 === 0 ? +cardDigit : 0;
            return sum;
        }, 0);

        return sumOfEvenPosition % 2 === 1;
    });
    return cardsWithSumofEvenPositionOdd;

});

const getsCardsIssuedBeforeJune = ((cards) => {
    const cardsIssuedBeforeJune = cards.filter((card) => {

        const [month, day, year] = card['issue_date'].split('/');

        return month < 6;
    });

    return cardsIssuedBeforeJune;
});

const assignCVV = ((cards) => {
    const cvvAssignedCards = cards.map((card) => {
        const MAX = 1000, min = 100;
        const cvv = Math.floor(Math.random() * (999 - 100) + 100).toString();

        return { ...card, cvv: cvv };
    });

    return cvvAssignedCards;
});

const assignValidationField = ((cards) => {
    const cardsWiValidationFiled = cards.map((card) => {

        return { ...card, validation: 'valid' };
    });
    return cardsWiValidationFiled;
});

const invalidateCardsBeforeMarch = ((cards) => {
    const cardsWiValidationFiled = cards.filter((card) => {
        const [month, day, year] = card['issue_date'].split('/');

        return month<3;
    }).map((cardsBeforeMarch) => {

        return { ...cardsBeforeMarch, validation: 'invalid' };
    });
    return cardsWiValidationFiled;
});

const sortCardsOnIssuedDates = ((cards) => {
    const  sortedCards= cards.sort((cardA,cardB) => {
        return +cardA['issue_date'].split('/')[2] - +cardB['issue_date'].split('/')[2];
    }).sort((cardA,cardB) => {
        return +cardA['issue_date'].split('/')[0] - +cardB['issue_date'].split('/')[0];
    }).sort((cardA,cardB) => {
        return +cardA['issue_date'].split('/')[1] - +cardB['issue_date'].split('/')[1];
    })
    return sortedCards;
});



